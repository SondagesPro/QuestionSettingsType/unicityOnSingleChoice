# unicityOnSingleChoice

LimeSurvey plugin to allow only one answer choosen in a dropdown question type

## How to use

After activation, you can set unicity on single choice dropdown question in advanced question setting.

If unicity was activated : number of already submitted response are checked, and answer already set are removed or set as disable.

This contol are only done when loading response and for submitted response. **This does not ensure a complete control over the uniqueness of the responses**.

## Installation

This plugin **need** [toolsDomDocument plugin](https://extensions.sondages.pro/toolssmartdomdocument) activated.

You can use [responseListAndManage plugin](https://gitlab.com/SondagesPro/managament/responseListAndManage) for group of token.

To setup this plugin like any other plugin in LimeSurvey : [you can follow this instructions](https://extensions.sondages.pro/install-and-activate-a-plugin-for-limesurvey.html).

## Home page and copyright
- HomePage <https://extensions.sondages.pro/>
- Copyright © 2020 Denis Chenu <https://sondages.pro>
- Licence : GNU General Public License <https://www.gnu.org/licenses/gpl-3.0.html>
