<?php
/**
 * unicityOnSingleChoice : a LimeSurvey plugin to allow only one answer choosen in a dropdown question type
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2019-2022 Denis Chenu <http://www.sondages.pro>
 * @license GPL v3
 * @version 0.5.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class unicityOnSingleChoice extends PluginBase
{
    protected $storage = 'DbStorage';

    protected static $name = 'unicityOnSingleChoice';
    protected static $description = 'Allow only one answer choosen in a dropdown question type. Token compatible, need toolsDomDocument and use responseListAndManage plugins.';

    /**
    * @var array[] the settings
    */
    protected $settings = array(
        'havetoolsDomDocument' => array(
            'type' => 'info',
            'content' => '',
        ),
        'haveresponseListAndManage' => array(
            'type' => 'info',
            'content' => '',
        ),
    );

    public function init()
    {
        $this->subscribe('beforeQuestionRender', 'removeCompletedChoice');
        $this->subscribe('newQuestionAttributes', 'addSingleAnswerOnSingleChoice');

        $this->subscribe('beforeActivate', 'beforeActivate');
    }

    /**
     * Show an alert if toolsSmartDomDocument is not here
     */
    public function beforeActivate()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $oToolsSmartDomDocument = Plugin::model()->find("name=:name", array(":name"=>'toolsDomDocument'));
        if (!$oToolsSmartDomDocument) {
            $this->getEvent()->set('message', $this->gT("You must download toolsDomDocument plugin"));
            $this->getEvent()->set('success', false);
        } elseif (!$oToolsSmartDomDocument->active) {
            $this->getEvent()->set('message', $this->gT("You must activate toolsDomDocument plugin"));
            $this->getEvent()->set('success', false);
        }
    }

    public function removeCompletedChoice()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        if (!Yii::getPathOfAlias('toolsDomDocument')) {
            return;
        }
        if ($this->getEvent()->get('type')=='!') {
            $oEvent=$this->getEvent();
            $surveyId = $oEvent->get('surveyId');
            $oSurvey = Survey::model()->findByPk($surveyId);
            if (!$oSurvey->getIsActive()) {
                return;
            }
            if (empty($_SESSION['survey_'.$surveyId]['srid'])) {
                return;
            }
            $srid = $_SESSION['survey_'.$surveyId]['srid'];
            $qid = $oEvent->get('qid');
            $oSingleAnswerAttribute = QuestionAttribute::model()->find(
                "attribute=:attribute and qid=:qid",
                array(':attribute'=>'unicityOnChoice',':qid'=>$qid)
            );
            if (empty($oSingleAnswerAttribute) || empty($oSingleAnswerAttribute->value)) {
                return;
            }
            $aAttributes = QuestionAttribute::model()->getQuestionAttributes($qid);
            $unicityOnChoiceWay = $aAttributes['unicityOnChoiceWay'];
            $unicityOnChoiceToken = $aAttributes['unicityOnChoiceToken'];
            $unicityOnChoiceCheckEmpty = $aAttributes['unicityOnChoiceCheckEmpty'];
            $unicityOnChoiceAltColumn = $aAttributes['unicityOnChoiceAltColumn'];
            $unicityOnChoiceExtra = $aAttributes['unicityOnChoiceExtra'];
            $unicityOnChoiceOthers = null;
            if(!empty($aAttributes['unicityOnChoiceOthers'])) {
                $unicityOnChoiceOthers = $aAttributes['unicityOnChoiceOthers'];
            }
            $token = null;
            if ($unicityOnChoiceToken && !$oSurvey->getIsAnonymized()) {
                // If not empty : survey is not anonymous and have token. Adding control on Survey in case of future issue
                $token = \LimeExpressionManager::ProcessStepString("{TOKEN}");
                $tokenList = null;
                if ($unicityOnChoiceToken == 'group' && App()->getConfig('TokenUsersListAndManageAPI')) {
                    $tokenList = \TokenUsersListAndManagePlugin\Utilities::getTokensList($surveyId, $token);
                }
            }
            $baseId = $oEvent->get('surveyId')."X".$oEvent->get('gid')."X".$oEvent->get('qid');
            /* get the answers */
            if (intval(App()->getConfig('versionnumber')) < 4 ) {
                $oAnswers = Answer::model()->findAll("qid = :qid AND language = :language", array(":qid" => $qid, ":language" => App()->getLanguage()));
            } else {
                $oAnswers = Answer::model()->findAll("qid = :qid ", array(":qid" => $qid));
            }
            /* Extra to be removed */
            $aAnswerCodesExtra = $this->_getExtraAnswers($unicityOnChoiceExtra);
            $aAnswerCodesExtra = array_intersect($aAnswerCodesExtra, CHtml::listData($oAnswers,'code','code'));
            /* Done to be removed */
            $aAnswerCodesDone = array();
            foreach ($oAnswers as $oAnswer) {
                $oCriteria = new CDbCriteria();
                $getColumnFilter = $this->_getColumnFilter($surveyId, $unicityOnChoiceCheckEmpty, 'submitdate');
                if(!empty($getColumnFilter)) {
                    $getColumnFilter = App()->getDb()->quoteColumnName($getColumnFilter);
                    $oCriteria->condition = "$getColumnFilter IS NOT NULL AND $getColumnFilter <>'' ";
                }
                $oCriteria->compare('id', "<>{$srid}");
                if(!empty($unicityOnChoiceOthers)) {
                    $extraFilters = \getQuestionInformation\Utilities::getFinalOtherFilter($surveyId, $unicityOnChoiceOthers);
                    foreach($extraFilters as $column => $value) {
                        $quotedColumn = Yii::app()->db->quoteColumnName($column);
                        if(empty($value)) {
                            $oCriteria->addCondition("$quotedColumn IS NULL OR $quotedColumn =''");
                        } else {
                            $value = \LimeExpressionManager::ProcessStepString($value, array(), 3, true);
                            $oCriteria->compare($quotedColumn, $value);
                        }
                    }
                }
                $altColumn = $this->_getColumnFilter($surveyId, $unicityOnChoiceAltColumn);
                if($altColumn) {
                    $oCriteria->compare(Yii::app()->db->quoteColumnName($altColumn), $oAnswer->code);
                } else {
                    $oCriteria->compare(Yii::app()->db->quoteColumnName($baseId), $oAnswer->code);
                }
                if (!empty($tokenList)) {
                    $oCriteria->addInCondition('token', $tokenList);
                } elseif (!empty($token)) {
                    $oCriteria->compare('token', $token);
                }
                if (Response::model($surveyId)->count($oCriteria) >= 1) {
                    $aAnswerCodesDone[] = $oAnswer->code;
                }
            }
            if (empty($aAnswerCodesDone) && empty($aAnswerCodesExtra)) {
                return;
            }
            $dom = new \toolsDomDocument\SmartDOMDocument();
            $dom->loadPartialHTML($oEvent->get('answers'));
            $baseSelectId = "answer".$baseId;
            $select = $dom->getElementById($baseSelectId);
            $domElemsToRemove = array(); 
            foreach ($select->childNodes as $element) {
                if ($element->nodeName == 'option') {
                    if (in_array($element->getAttribute('value'), $aAnswerCodesDone) || in_array($element->getAttribute('value'), $aAnswerCodesExtra)) {
                        if ($unicityOnChoiceWay == 'disable') {
                            $element->setAttribute('disabled',true);
                        } else {
                            $domElemsToRemove[] = $element;
                        }
                    }
                }
            }
            foreach( $domElemsToRemove as $domElement ){
                $domElement->parentNode->removeChild($domElement);
            } 
            $newHtml = $dom->saveHTMLExact();
            $oEvent->set('answers', $newHtml);
            if (count($aAnswerCodesDone) == count($oAnswers)) {
                $oEvent->set('valid_message', "<div class='text-danger unicity-single-choice-alldone'>".$this->gT("All the options for this question have already been chosen.")."</div>");
            }
        }
    }

    /**
     * @see parent:getPluginSettings
     */
    public function getPluginSettings($getValues = true)
    {
        if(!Permission::model()->hasGlobalPermission('settings','read')) {
            throw new CHttpException(403);
        }
        $pluginSettings= parent::getPluginSettings($getValues);
        if (!Yii::getPathOfAlias('toolsDomDocument')) {
            $pluginSettings['havetoolsDomDocument']['content'] = sprintf(
                $this->gT("You must activate %stoolsDomDocument%s plugin"),
                "<a href='https://extensions.sondages.pro/toolssmartdomdocument'>",
                "</a>"
            );
            $pluginSettings['havetoolsDomDocument']['class'] = "alert alert-danger";
        } else {
            $pluginSettings['havetoolsDomDocument']['content'] = $this->gT("You have toolsDomDocument plugin");
            $pluginSettings['havetoolsDomDocument']['class'] = "alert alert-success";
        }
        if (!Yii::getPathOfAlias('responseListAndManage')) {
            $pluginSettings['haveresponseListAndManage']['content'] = sprintf(
                $this->gT("You must activate or update %sresponseListAndManage%s plugin (minimum version : 1.15) to use token groups (optionnal)."),
                "<a href='https://gitlab.com/SondagesPro/managament/responseListAndManage'>",
                "</a>"
            );
            $pluginSettings['haveresponseListAndManage']['class'] = "alert alert-warning";
        } else {
            $pluginSettings['haveresponseListAndManage']['content'] = $this->gT("You have responseListAndManage up to date");
            $pluginSettings['haveresponseListAndManage']['class'] = "alert alert-success";
        }
        return $pluginSettings;
    }

    public function addSingleAnswerOnSingleChoice()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        if (!Yii::getPathOfAlias('toolsDomDocument')) {
            return;
        }
        $addFilterHtmlAttribute = array(
            'unicityOnChoice' => array(
                'name'      => 'unicityOnChoice',
                'types'     => "!",
                'category'  => gT('Unicity'),
                'sortorder' => 201,
                'inputtype' => 'switch',
                'options'=>array(
                    0=>gT('No'),
                    1=>gT('Yes')
                ),
                'caption'   => $this->gT('Unicity on choice'),
                'help'      => $this->gT('Unicity control was done with removing or didable choice already done.'),
                'default'   => 0,
            ),
            'unicityOnChoiceWay' => array(
                'name'      => 'unicityOnChoiceWay',
                'types'     => "!",
                'category'  => gT('Unicity'),
                'sortorder' => 201,
                'inputtype' => 'singleselect',
                'options'=>array(
                    'remove'=>$this->gT('Remove'),
                    'disable'=>$this->gT('Disable'),
                ),
                'caption'   => $this->gT('Way to daible choice'),
                'help'      => $this->gT('Removing the option from the list or disable the option in the dropdown.'),
                'default'   => 'remove',
            ),
            'unicityOnChoiceToken' => array(
                'name'      => 'unicityOnChoiceToken',
                'types'     => "!",
                'category'  => gT('Unicity'),
                'sortorder' => 202,
                'inputtype' =>'singleselect',
                'options'=>array(
                    'no'=>gT('No'),
                    'token'=>gT('Token'),
                    'group'=>gT('Token Group (with responseListAndManage plugin)')
                ),
                'caption'   => $this->gT('Usage of token'),
                'help'      => $this->gT('Did token are checked when checling unicity. Usable inside not anonymous survey only.'),
                'default'   => 'token',
            ),
            'unicityOnChoiceCheckEmpty' => array(
                'name'      => 'unicityOnChoiceToken',
                'types'     => "!",
                'category'  => gT('Unicity'),
                'sortorder' => 210,
                'inputtype' =>'text',
                'expression'=> 1,
                'caption'   => $this->gT('Filter previous response by (default to submitdate)'),
                'help'      => $this->gT('By default use only submitted data, you can use all response (even no submitted) set to an invalid column. To use a question code, you can use <code>{QCode.sgqa}</code>.'),
                'default'   => '',
            ),
            'unicityOnChoiceAltColumn' => array(
                'name'      => 'unicityOnChoiceAltColumn',
                'types'     => "!",
                'category'  => gT('Unicity'),
                'sortorder' => 220,
                'inputtype' =>'text',
                'expression'=> 1,
                'caption'   => $this->gT('Alternate column to find choice already done'),
                'help'      => $this->gT('By default use the column of this question,. To use another question : you can use <code>{QCode.sgqa}</code>.'),
                'default'   => '',
            ),
            'unicityOnChoiceExtra' => array(
                'name'      => 'unicityOnChoiceExtra',
                'types'     => "!",
                'category'  => gT('Unicity'),
                'sortorder' => 230,
                'inputtype' =>'textarea',
                'expression'=> 1,
                'caption'   => $this->gT('Extra answer to be removed (comma separated)'),
                'help'      => $this->gT('You can remove any extra answer from current questionaire, Expression allowed.'),
                'default'   => '',
            ),
            'unicityOnChoiceOthers' => array(
                'name'      => 'unicityOnChoiceOthers',
                'types'     => "!",
                'category'  => gT('Unicity'),
                'sortorder' => 240,
                'inputtype' =>'textarea',
                'expression'=> 1,
                'caption'   => $this->gT('Other question fields added as filter.'),
                'help'      => $this->gT('One field by line, field must be a valid question code (single question only). Field and value are separated by colon (<code>:</code>), you can use Expressiona Manager in value. If value is an empty string (before expression), filter use empty for value.'),
                'default'   => '',
            ),
        );
        if (!App()->getConfig('TokenUsersListAndManageAPI')) {
            unset($addFilterHtmlAttribute['unicityOnChoiceToken']['options']['group']);
        }
        if (method_exists($this->getEvent(), 'append')) {
            $this->getEvent()->append('questionAttributes', $addFilterHtmlAttribute);
        } else {
            $questionAttributes=(array)$this->event->get('questionAttributes');
            $questionAttributes=array_merge($questionAttributes, $addFilterHtmlAttribute);
            $this->event->set('questionAttributes', $questionAttributes);
        }
    }

    /**
     * Get the column corresponding to a $string
     * @param integer $surveyId
     * @param string|null $string
     * @param null|string $default default column to be used
     * @return string
     */
    private function _getColumnFilter($surveyId, $string, $default =  null)
    {
        if(is_null($string)) {
            return $default;
        }
        $string = trim(\LimeExpressionManager::ProcessStepString($string, array() ,3 , true ));
        if(empty($string)) {
            return $default;
        }
        $availableColumns = SurveyDynamic::model($surveyId)->getAttributes();
        if (array_key_exists($string, $availableColumns)) {
            return $string;
        }
        // To log or throw error (if user can update it)
        return "";
    }

    /**
     * Get a list of potential answers code to be removed
     * @param string$string
     * @return string[]
     **/
    private function _getExtraAnswers($string)
    {
        if(empty($string)) {
            return array();
        }
        $string = trim(\LimeExpressionManager::ProcessStepString($string, array() ,3 , true ));
        if(empty($string)) {
            return array();
        }
        $aAnswer = explode(",",$string);
        $aAnswer = array_map(function($code) {
            return filter_var($code, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH + FILTER_FLAG_STRIP_LOW);
        }, $aAnswer);
        array_filter($aAnswer);
        array_unique($aAnswer);
        return $aAnswer;
    }
}
